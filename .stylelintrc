rules:
    color-hex-case: "upper"
    color-hex-length: "long"
    color-named: "never"
    # color-no-hex: false
    color-no-invalid-hex: true

    font-family-name-quotes: "always-unless-keyword"

    font-weight-notation: "numeric"

    function-blacklist: []
    function-calc-no-unspaced-operator: true
    function-comma-newline-after: "always-multi-line"
    function-comma-newline-before: "never-multi-line"
    function-comma-space-after: "always-single-line"
    function-comma-space-before: "never"
    function-linear-gradient-no-nonstandard-direction: true
    function-max-empty-lines: 0
    function-name-case: "lower"
    function-parentheses-newline-inside: "always-multi-line"
    function-parentheses-space-inside: "never-single-line"
    function-url-data-uris: "never"
    function-url-quotes: "always"
    # function-whitelist: false
    function-whitespace-after: "always"

    number-leading-zero: "never"
    number-max-precision: 4
    number-no-trailing-zeros: true
    number-zero-length-no-unit: true

    string-no-newline: true
    string-quotes: "single"

    time-no-imperceptible: true

    unit-blacklist: []
    unit-case: "lower"
    unit-no-unknown: true
    # unit-whitelist: false

    value-no-vendor-prefix: true

    value-keyword-case: "lower"
    value-list-comma-newline-after: "always-multi-line"
    value-list-comma-newline-before: "never-multi-line"
    value-list-comma-space-after: "always-single-line"
    value-list-comma-space-before: "never"

    custom-property-no-outside-root: true
    # custom-property-pattern: false

    shorthand-property-no-redundant-values: true

    property-blacklist: []
    property-case: "lower"
    property-no-vendor-prefix: true
    property-unit-blacklist: []
    # property-unit-whitelist: false
    # property-value-blacklist: []
    # property-value-whitelist: false
    # property-whitelist: false

    declaration-bang-space-after: "never"
    declaration-bang-space-before: "always"
    declaration-colon-newline-after: "always-multi-line"
    declaration-colon-space-after: "always-single-line"
    declaration-colon-space-before: "never"
    declaration-no-important: true

    declaration-block-no-duplicate-properties: true
    declaration-block-no-ignored-properties: true
    declaration-block-no-shorthand-property-overrides: true
    # declaration-block-properties-order: false
    declaration-block-semicolon-newline-after: "always"
    declaration-block-semicolon-newline-before: "never-multi-line"
    declaration-block-semicolon-space-after: "always-single-line"
    declaration-block-semicolon-space-before: "never"
    declaration-block-single-line-max-declarations: 1
    declaration-block-trailing-semicolon: "always"

    block-closing-brace-newline-after: "always"
    block-closing-brace-newline-before: "always"
    # block-closing-brace-space-after: false
    # block-closing-brace-space-before: false
    # block-no-empty: false
    block-no-single-line: true
    block-opening-brace-newline-after: "always"
    block-opening-brace-newline-before: "never-single-line"
    # block-opening-brace-space-after: "never"
    block-opening-brace-space-before: "always"

    selector-attribute-brackets-space-inside: "never"
    # selector-class-pattern: false
    selector-combinator-space-after: "always"
    selector-combinator-space-before: "always"
    # selector-id-pattern: false
    selector-max-specificity: "0,3,1"
    # selector-no-attribute: true
    # selector-no-combinator: false
    selector-no-id: true
    selector-no-qualifying-type: true
    selector-no-universal: true
    selector-no-vendor-prefix: true
    selector-pseudo-class-case: "lower"
    selector-pseudo-class-parentheses-space-inside: "never"
    selector-pseudo-element-case: "lower"
    selector-pseudo-element-colon-notation: "double"
    selector-root-no-composition: true
    selector-type-case: "lower"

    selector-list-comma-newline-after: "always-multi-line"
    selector-list-comma-newline-before: "never-multi-line"
    selector-list-comma-space-after: "always-single-line"
    selector-list-comma-space-before: "never"

    root-no-standard-properties: true

    rule-nested-empty-line-before: ["always", { except: ["first-nested"], ignore: ["after-comment"] }]
    rule-non-nested-empty-line-before: "always"

    media-feature-colon-space-after: "always"
    media-feature-colon-space-before: "never"
    media-feature-name-no-vendor-prefix: true
    media-feature-no-missing-punctuation: true
    media-feature-range-operator-space-after: "always"
    media-feature-range-operator-space-before: "always"

    # custom-media-pattern: false

    media-query-parentheses-space-inside: "never"

    media-query-list-comma-newline-after: "always-multi-line"
    media-query-list-comma-newline-before: "never-multi-line"
    media-query-list-comma-space-after: "always-single-line"
    media-query-list-comma-space-before: "never"

    at-rule-empty-line-before: ["always", {except: ["blockless-group", "first-nested"], ignore: ["after-comment"]}]
    at-rule-name-case: "lower"
    at-rule-no-vendor-prefix: true
    at-rule-semicolon-newline-after: "always"

    comment-empty-line-before: ["always", {ignore: ["between-comments"]}]
    comment-whitespace-inside: "always"
    # comment-word-blacklist: []

    indentation: "tab"
    max-empty-lines: 1
    max-line-length: 120
    max-nesting-depth: 3
    no-browser-hacks: true
    # no-descending-specificity: true
    no-duplicate-selectors: true
    no-eol-whitespace: true
    # no-indistinguishable-colors: true
    no-invalid-double-slash-comments: true
    no-missing-eof-newline: true
    no-unknown-animations: true
    # no-unsupported-browser-features: true
    stylelint-disable-reason: "always-after"
