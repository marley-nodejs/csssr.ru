import React from 'react';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import cx from 'classnames';

import styles from './styles.css';

function Options(props) {
	const blockClass = cx({
		[styles.root]: true,
		[styles.root_inline]: props.inline,
		[props.className]: props.className,
	});

	return (
		<ul {...props} className={blockClass}>
			{props.children.map((child, index) => (
				<li className={styles.item} key={index}>
					{child}
				</li>
			))}
		</ul>
	);
}

Options.propTypes = {
	children: React.PropTypes.node,
	className: React.PropTypes.string,
	inline: React.PropTypes.bool,
};

Options.defaultProps = {
	inline: false,
};

export default withStyles(Options, styles);
