import React from 'react';
import Hole from './index.jsx';
import storiesOf from 'utils/storiesOf';

storiesOf('Hole')
	.add('default', () => (
		<Hole />
	));
