export const index = {
	title: 'Космический фронтенд – CSSSR',
	meta: [
		{
			name: 'description',
			content: 'CSSSR — веб-студия, предоставляющая frontend аутсорсинг. Мы верстаем из PSD в HTML и CSS. Вёрстка для сайтов, лэндингов и почтовых рассылок.',
			pageKeywords: 'CSSSR, CSSR, css-service, вёрстка, вёрстка сайтов, вёрстка PSD в HTML, PSD to HTML, frontend аутсорсинг, фронтенд',
		},
		{
			name: 'keywords',
			content: 'CSSSR, CSSR, css-service, вёрстка, вёрстка сайтов, вёрстка PSD в HTML, PSD to HTML, frontend аутсорсинг, фронтенд',
		},
		{
			name: 'og:title',
			content: 'CSSSR — Вёрстка и JavaScript программирование',
		},
		{
			name: 'og:url',
			content: 'http://csssr.ru/',
		},
		{
			name: 'og:description',
			content: 'Фронтенд разработка сайтов и веб-приложений',
		},
		{
			name: 'og:image',
			content: 'http://csssr.ru/share/frontend-with-space-speed.jpg',
		},
	],
};

export const company = {
	title: 'О компании',
	meta: [
		{
			name: 'description',
			content: 'Фронтенд разработка сайтов и веб-приложений',
		},
		{
			name: 'keywords',
			content: 'CSSSR, ЦСССР, CSSR, ЦССР',
		},
	],
};

export const jobs = {
	title: 'Вакансии CSSSR — Удалённая работа, полная страданий, боли и отчаяния',
	meta: [
		{
			name: 'description',
			content: 'CSSSR — Работай, где хочешь!',
		},
		{
			name: 'og:title',
			content: 'Вакансии CSSSR — Удалённая работа, полная страданий, боли и отчаяния',
		},
		{
			name: 'og:description',
			content: 'CSSSR — Работай, где хочешь!',
		},
		{
			name: 'og:url',
			content: 'http://csssr.ru/jobs/',
		},
		{
			name: 'og:image',
			content: 'http://csssr.ru/share/remote-work.jpg',
		},
	],
};

export const job = {
	'pixel-perfectionist': {
		title: 'Вакансия «Верстальщик пиксель-перфекционист»',
		meta: [
			{
				name: 'og:title',
				content: 'Вакансия «Верстальщик пиксель-перфекционист»',
			},
			{
				name: 'og:url',
				content: 'http://csssr.ru/jobs/pixel-perfectionist',
			},
		],
	},
	'technical-manager': {
		title: 'Вакансия «Менеджер-технарь»',
		meta: [
			{
				name: 'og:title',
				content: 'Вакансия «Менеджер-технарь»',
			},
			{
				name: 'og:url',
				content: 'http://csssr.ru/jobs/technical-manager',
			},
		],
	},
};

export const order = {
	title: 'Вёрстка проекта в CSSSR',
	meta: [
		{
			name: 'description',
			content: 'Заказ вёрстки сайта из PSD-макетов в HTML с гарантией дедлайна',
		},
		{
			name: 'keywords',
			content: 'CSSSR, вёрстка сайта, из PSD в HTML, PSD to HTML, калькулятор вёрстки, стоимость вёрстки, заказ вёрстки, калькулятор CSSSR, ЦСССР, CSSR',
		},
	],
};

export const outsource = {
	title: 'CSSSR — фронтенд аутсорсинг. Что угодно с помощью HTML, CSS, JavaScript.',
	meta: [
		{
			name: 'description',
			content: 'CSSSR — это фронтенд аутсорсинг: HTML, CSS, JavaScript, AngularJS',
		},
		{
			name: 'keywords',
			content: 'CSSSR, CSSR, css-service, вёрстка, вёрстка сайтов, вёрстка PSD в HTML, PSD to HTML, frontend аутсорсинг, фронтенд, поддержка, рефакторинг, HTML,CSS, JavaScript, JS, Angular, AngularJS',
		},
	],
};

export const portfolio = {
	title: 'Портфолио CSSSR',
};

export const offert = {
	title: 'Общие условия оказания услуг',
};

export const confidential = {
	title: 'Положение об обработке персональных данных',
};

export const timeline = {
	...index,
	title: 'История полёта',
	meta: [
		{
			name: 'description',
			content: 'Таймлайн',
		},
	],
};

export const thanks = {
	title: 'Успех, товарищ!',
};
